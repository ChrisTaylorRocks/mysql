Function Invoke-MySQLCmd {
    <#
        Requires MySQL .NET Connector to work
        https://dev.mysql.com/downloads/connector/net/

        Author: Chris Taylor
    #>
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        [string]$Server,
        [Parameter(Mandatory = $true)]
        [string]$UserName,
        [Parameter(Mandatory = $true)]
        [string]$Password,
        [Parameter(Mandatory = $true)]
        [string]$Database,
        [Parameter(Mandatory = $true)]
        [string]$Query,
        [hashtable]$Parameters,
        [string]$Port = 3306
    )

    $ConnectionString = "server=$($Server);port=$($Port);uid=$($UserName);pwd=$($Password);database=$($Database);convert zero datetime=True"

    Try {
        [void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")
        $Connection = New-Object MySql.Data.MySqlClient.MySqlConnection
        $Connection.ConnectionString = $ConnectionString
        $Connection.Open()

        $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($Query, $Connection)

        if ($Parameters) {
            foreach($P in $Parameters.GetEnumerator()){
                $Key = $P.Name
                [regex]$Regex = '^@\w*'
                if(!$Regex.match($Key).Success){ $Key = "@$($P.Name)" }
                $null = $Command.Parameters.AddWithValue($Key, $P.Value)
            }
            Write-Verbose ($Command.Parameters | Out-String)
        }        

        $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command)
        $DataSet = New-Object System.Data.DataSet
        $RecordCount = $dataAdapter.Fill($dataSet, "data")
        $DataSet.Tables[0]
    }
    Catch {
        Write-Error "ERROR : Unable to run query : $query `n$($_)"
    }
    Finally {
        $Connection.Close()
    }
}
